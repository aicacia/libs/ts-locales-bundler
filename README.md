# ts-locales-bundler

locales bundler

```
locales/
  en/
    app.json
    error.json
  de/
    app.json
    error.json
```

```javascript
const gulp = require("gulp"),
  localesBundler = require("@aicacia/locales-bundler");

gulp.task("locales", () =>
  gulp
    .src("locales")
    .pipe(localesBundler({ flatten: true, minify: false }))
    .pipe(gulp.dest("build/locales"))
);
```
